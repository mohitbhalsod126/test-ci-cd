import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment as env } from '../environments/environment';
import { of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private httpClient: HttpClient) { }

  title = 'CI-CD Test App';

  back_end_path = `${env.endPoint}/api/hello`;

  apiCall$ = this.httpClient.get<{ msg: string }>(this.back_end_path)
    .pipe(
      catchError(err => {
        return of(err.message);
      }),
      map(res => res.msg)
    );
}

const http = require('http')
const express = require('express')
const app = express()
const port = 3000
const cors = require('cors')

app.use(cors())

app.get('*', (_req, res) => {
  res.json({ msg: 'Version 3.0' })
})

app.set('port', port)

const server = http.createServer(app)

server.listen(port)

server.on('error', onError);
server.on('listening', onListening);

// Event listener for HTTP server "error" event.
function onError(error) {
  console.log('Error: ', error);
}

// Event listener for HTTP server "listening" event.
function onListening() {
  var addr = server.address();
  console.log('test-app-back-end listening on port:' + addr.port);
}

